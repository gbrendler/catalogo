import styles from '../../styles/pages/collections/Index.module.css'
import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from 'next/router'
import response from '../../products.json'

export default function getProduct(props) {

  const router = useRouter()

  const product_id = router.query.product
  const collection_id = router.query.collection
  let products_array = []

  if (collection_id == 'G1') {
    products_array = response.G1
  }

  else if (collection_id == 'G2') {
    products_array = response.G2
  }

  else if (collection_id == 'GG') {
    products_array = response.GG
  }

  else if (collection_id == 'G') {
    products_array = response.G
  }

  else if (collection_id == 'M') {
    products_array = response.M
  }

  else if (collection_id == 'P') {
    products_array = response.P
  }

  const product = products_array.map(product => {
    if (product.id == product_id) {
      return (
        <>
          <div className={styles.productsHeader}>
            <h3>{product.name}</h3>
            <button onClick={() => back()}>Voltar </button>
          </div>
          <div className={styles.productContainer}>
            <Image src={product.image} width="500px" height="600px" />
            <div className={styles.info}>
              <p className={styles.price}>{product.price}</p>
              <p>Nº de identificação: {product.id}</p>
            </div>
          </div>
        </>
      )
    }
  })

  function back() {
    window.history.back()
  }

  return (
    <div className={styles.container}>
      {product}
    </div>
  )
}