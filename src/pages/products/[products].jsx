import styles from '../../styles/pages/collections/products/Index.module.css'
import Link from 'next/link'
import { useRouter } from 'next/router'
import Products from '../../components/Products'

export default function getProducts() {

  const router = useRouter()
  const { products } = router.query
  const collection = products

  function back() {
    window.history.back()
  }

  //buscar produtos através de collection

  return (
    <div className={styles.container}>
      {/* <div className={styles.navegationLinks}>
        <Link href="/">Início</Link>
      </div> */}
      <div className={styles.productsHeader}>
        <h3>Camisetas {collection}</h3>
        <div>
          <button onClick={() => back()}>Voltar </button>
        </div>

      </div>

      <Products collection={collection} />

    </div>
  )
}