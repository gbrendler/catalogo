import Collections from "../components/Collections";
import styles from '../styles/pages/Index.module.css'
import Image from 'next/image'

export default function Home() {

  return (
    <>
      <div className={styles.productsContainer}>
        <Collections />
      </div>

      <div className={styles.modal} >
        <div className={styles.content_modal}>
          <p>Sui Generis</p>
          <Image src="/logo.svg" width="350px" height="237px" layout="fixed" />
          <p>Incomparável, assim como você</p>
        </div>
      </div>
    </>
  )
}
