
import styles from '../styles/components/Footer.module.css'


export default function Footer() {
  return (
    <div className={styles.footerContainer}>
      <div>
        <p>Direitos autorais © 2021, Suigeneris.</p>
      </div>
    </div>
  )
}