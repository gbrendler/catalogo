import styles from '../styles/components/Collections.module.css'
import { useRouter } from 'next/router'
import Image from 'next/image'
import response from '../collections.json'

export default function Collections() {

  const router = useRouter()

  const collections = response.map(collection => {
    return (
      <>
        <div className={styles.collection} onClick={() => router.push(`products/${collection.name}`)}>
          <Image src={collection.image} width="250px" height="288px"/>
          <span>Camisetas {collection.name}</span>
        </div>
      </>)
  })

  return (

    <div className={styles.collectionsContainer}>
      {collections}
    </div>
  )
}