import styles from '../styles/components/Collections.module.css'
import { useRouter } from 'next/router'
import Image from 'next/image'
import response from '../products.json'

export default function Products(props) {

  const router = useRouter()

  const { collection } = props
  let products_array = []

  if (collection == 'G1') {
    products_array = response.G1
  }

  else if (collection == 'G2') {
    products_array = response.G2
  }

  else if (collection == 'GG') {
    products_array = response.GG
  }

  else if (collection == 'G') {
    products_array = response.G
  }

  else if (collection == 'M') {
    products_array = response.M
  }

  else if (collection == 'P') {
    products_array = response.P
  }

  const products = products_array.map(product => {

    function handleClick(e) {
      e.preventDefault()
      router.push({
        pathname: `/product/${product.id}`,
        query: { collection: collection }
      })

    }

    return (
      <div className={styles.product} key={product.id} onClick={handleClick}>
        <Image src={product.image} width={500} height={500} />
        <span>{product.name}</span>
        <p>{product.price}</p>
      </div>
    )
  })

  return (

    <div className={styles.productsContainer} collection={collection}>
      {products}
    </div>
  )
}