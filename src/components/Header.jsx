import styles from '../styles/components/Header.module.css'
// import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import InstagramIcon from '@material-ui/icons/Instagram';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import Link from 'next/link'
import Image from 'next/image'

export default function Header() {
  return (
    <div className={styles.HeaderContainer}>
      <div>
        <div></div>
        <div className={styles.HeaderLogo}>
          <Link href="/">
            <strong>Sui Generis</strong>
          </Link>
        </div>
        <div className={styles.socialMedia}>
          <div>
            <p>Faça seu Pedido</p>
            <a target="_blank" href="https://instagram.com/suige.neris2/">
              <InstagramIcon />
            </a>
            <a target="_blank" href="https://api.whatsapp.com/send?phone=5553999958283&text=Ol%C3%A1%2C%20eu%20gostaria%20de%20realizar%20um%20pedido.">
              <WhatsAppIcon />
            </a>
          </div>
        </div>
        {/* <div className={styles.HeaderCart}>
          <p>Carrinho</p>
          <ShoppingCartIcon />
        </div> */}
      </div>

    </div>
  )
}